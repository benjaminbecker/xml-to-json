# xml-json converter
Convert xml files to json files.

# Install
Download binary for 
[Windows](https://gitlab.com/benjaminbecker/xml-to-json/-/jobs/artifacts/master/download?job=build-windows) 
or 
[Linux](https://gitlab.com/benjaminbecker/xml-to-json/-/jobs/artifacts/master/download?job=build-linux). Store this in a folder where it can be found by your OS.

# Use
## Linux:
Type in terminal:

```shell
xml_to_json path/to/some/file.xml path/to/generated/file.json
```

## Windows
Type in powershell or cmd:

```powershell
xml_to_json.exe path\to\some\file.xml path\to\generated\file.json
```

# Source Code
This program is implemented in Rust. It uses the crates `quickxml_to_serde` and `serde_json`.