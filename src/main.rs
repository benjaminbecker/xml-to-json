use std::fs;
use std::env;
use std::io;

extern crate serde_json;

use quickxml_to_serde;

struct FilePaths {
    inputfile: String,
    outputfile: String,
}

fn get_file_paths() -> Option<FilePaths> {
    let args: Vec<String> = env::args().collect();
    let inputfile = args.get(1)?.clone();
    let outputfile = args.get(2)?.clone();
    Some(FilePaths { inputfile, outputfile })
}

fn read_input_file(inputfile: &str) -> io::Result<String> {
    fs::read_to_string(&inputfile)
}

fn convert_to_json(content: String) -> std::result::Result<String, serde_json::Error> {
    let jsn = quickxml_to_serde::xml_string_to_json(content);
    let jsn_pretty = serde_json::to_string_pretty(&jsn.as_object());
    jsn_pretty
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let command_hint = "ERROR: Please use the following syntax: \nxml_to_json <inputfile> <outputfile>";
    let FilePaths { inputfile, outputfile } = match get_file_paths() {
        Some(c) => { c },
        None => {
            println!("{}", command_hint);
            return Ok(())
        }
    };
    
    let content = match read_input_file(&inputfile) {
        Ok(c) => { c },
        Err(_e) => {
            println!("Could not read input file: {}", &inputfile);
            return Ok(())
        }
    };
    
    let jsn_pretty = match convert_to_json(content) {
        Ok(c) => { c },
        Err(_e) => {
            println!("Could not convert input file to json",);
            return Ok(())
        }
    };
    println!("{}", &jsn_pretty);

    fs::write(
        outputfile,
        &jsn_pretty
    ).expect("Error during write.");

    Ok(())

}